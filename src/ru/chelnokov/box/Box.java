package ru.chelnokov.box;

public class Box {
    /**
     * Ширина
     */
    private double width;
    /**
     * Высота
     */
    private double height;
    /**
     * Глубина
     */
    private double depth;

    Box(double width, double depth, double height) {
        this.width = width;
        this.depth = depth;
        this.height = height;
    }

    @Override
    public String toString() {
        return "{" + "Длина =" + width + ", Ширина=" + depth + ", Высота =" + height + '}';
    }

    double boxCapacity() {
        return width * depth * height;
    }


}
