package ru.chelnokov.box;

import java.util.Scanner;

public class Main {
    private static Scanner reader = new Scanner(System.in);
    private static final String YOUR_FORMAT = "Объем вашей коробки %s равен %.2f %n";
    private static final String MY_FORMAT = "Объем моей коробки %s равен %.2f %n";

    public static void main(String[] args) {
        double width;
        double hight;
        double depht;
        do {
            System.out.println("Длина вашей коробки:");
            width = reader.nextDouble();
            System.out.println("Ширина вашей коробки:");
            depht = reader.nextDouble();
            System.out.println("Высота вашей коробки:");
            hight = reader.nextDouble();
            if (width <= 0 || hight <= 0 || depht <= 0) {
                System.out.println("Кажется, вы допустили ошибку, попробуйте ввести данные еще раз!");
            }
        } while (width <= 0 || depht <= 0|| hight <= 0 );
        Box box = new Box(width,depht , hight);
        double boxVolume = box.boxCapacity();
        System.out.printf(YOUR_FORMAT, box, boxVolume);
        Box myBox = new Box(5, 7, 4);
        double myBoxVolume = myBox.boxCapacity();
        System.out.printf(MY_FORMAT, myBox, myBoxVolume);
        if (boxVolume > myBoxVolume) {
            System.out.println("Объём Вашей коробки, чем моей ");
        } else if (boxVolume < myBoxVolume) {
            System.out.println("Объём моей коробки больше, чем объём вашей");
        } else {
            System.out.println("Наши коробки равны по объёму ");
        }
    }

}

